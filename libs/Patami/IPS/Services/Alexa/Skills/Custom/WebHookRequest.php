<?php
/**
 * Patami IPS Framework
 *
 * @package IPSPATAMI
 * @version 3.4
 * @link https://bitbucket.org/patami/ipspatami
 *
 * @author Florian Wiethoff <florian.wiethoff@patami.com>
 * @copyright 2017 Florian Wiethoff
 *
 * @license GPL
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * By intentionally submitting any modifications, corrections or derivatives to this work, or any other work intended
 * for use with this Software, to the author, you confirm that you are the copyright holder for those contributions and
 * you grant the author a nonexclusive, worldwide, irrevocable, royalty-free, perpetual, license to use, copy, create
 * derivative works based on those contributions, and sublicense and distribute those contributions and any derivatives
 * thereof.
 */


namespace Patami\IPS\Services\Alexa\Skills\Custom;


use Patami\IPS\Services\Alexa\Skills\Custom\Exceptions\ApplicationIdNotAllowedException;
use Patami\IPS\Services\Alexa\Skills\Custom\Exceptions\InvalidRequestIdException;
use Patami\IPS\Services\Alexa\Skills\Custom\Exceptions\InvalidRequestVersionException;
use Patami\IPS\Services\Alexa\Skills\Custom\Exceptions\InvalidSessionIdException;
use Patami\IPS\Services\Alexa\Skills\Custom\Exceptions\LaunchRequestIntentNotSetException;
use Patami\IPS\Services\Alexa\Skills\Custom\Exceptions\LocaleNotSupportedException;
use Patami\IPS\Services\Alexa\Skills\Custom\Exceptions\RequestTypeNotSupportedException;
use Patami\IPS\Services\Alexa\Skills\Custom\Exceptions\UserIdNotAllowedException;
use Patami\IPS\Services\Alexa\Skills\Custom\Exceptions\IntentNotFoundException;


/**
 * Class for requests from uncertified Amazon Alexa Custom Skills using User ID for authentication.
 *
 * You can use this class as a base class if you want to implement your own Alexa Custom Skills that you will only use
 * in your own IPS installation. If you want to distribute them to others (like we did with the example skills), then
 * users will need to setup the configuration in the Amazon Developer Console themselves. You also cannot certify them.
 * The main advantage is, that you do not need to certify them and that you don't need to ask the Symcon GmbH for a
 * OAuth token.
 *
 * @see WebOAuthRequest
 * @see WebHookIOModule
 *
 * @package IPSPATAMI
 */
class WebHookRequest extends Request
{

    /** @var WebHookIOModule WebHook I/O module object processing the request. */
    protected $io;

    /**
     * Validates the current request by checking various request data fields.
     * It verifies the user ID and calls the parent method to validate all other fields.
     * @throws InvalidRequestVersionException if the version is invalid.
     * @throws LocaleNotSupportedException if the locale is not known or supported.
     * @throws InvalidRequestIdException if the request ID is invalid.
     * @throws InvalidSessionIdException if the session ID is invalid.
     * @throws ApplicationIdNotAllowedException if the application ID is not allowed.
     * @throws RequestTypeNotSupportedException if the request type is not supported.
     * @throws UserIdNotAllowedException if the user ID is not allowed.
     */
    protected function Validate()
    {
        parent::Validate();

        $this->ValidateUserId();
    }

    /**
     * Validates the user ID field of the request against the allowed user ID.
     * The user ID needs to be configured on the I/O's configuration page.
     * @throws UserIdNotAllowedException if the user ID is not allowed.
     */
    protected function ValidateUserId()
    {
        // Validate User ID
        $id = @$this->data['session']['user']['userId'];
        $this->Debug('User ID Validation', $id);
        if ($id !=  $this->io->GetAllowedUserId()) {
            $this->Debug('User ID Validation', 'User ID is not allowed');
            throw new UserIdNotAllowedException();
        }
    }

    /**
     * Processes Alexa Custom Skill LaunchRequests and returns the response.
     * Checks if a LaunchRequest intent is configured on the WebHook I/O's configuration page and tries to execute it.
     * If none is configured, the parent method is called which tries to execute the built-in LaunchRequest handler
     * class.
     * @return Response Response object generated by the intent.
     * @throws IntentNotFoundException if the intent configured on the I/O's configuration page can not be found.
     * @throws LaunchRequestIntentNotSetException if the I/O does not return an intent class that handles LaunchRequests.
     */
    protected function ProcessLaunchRequest()
    {
        // Get the launch intent instance ID
        $instanceId = $this->io->GetLaunchIntentId();

        // Fall back to module intent if not set
        if (! $instanceId) {
            return parent::ProcessLaunchRequest();
        }

        // Create the intent instance object
        $intent = InstanceIntent::CreateByInstanceId($this->io, $instanceId);

        // Push the intent name to the stack
        $this->intentStack[] = $intent->GetName();

        // Execute the intent
        return $intent->Execute($this);
    }

    /**
     * Processes Alexa Custom Skill IntentRequests and returns the response.
     * Checks if a matching IPS intent instance can be found and executes if. If the user did not create one for the
     * specified intent name, the parent method is called which tries to find a built-in intent handler class.
     * @param string $intentName Intent name as received from the Amazon servers.
     * @return Response Response object generated by the intent.
     * @throws IntentNotFoundException if no matching intent could be found.
     */
    protected function ProcessIntentRequest($intentName)
    {
        try {
            // Create the intent instance object
            $intent = InstanceIntent::CreateByName($this->io, $intentName);

            // Push the intent name to the stack
            $this->intentStack[] = $intent->GetName();
        } catch (IntentNotFoundException $e) {
            // Fall back to module intents
            return parent::ProcessIntentRequest($intentName);
        }

        // Execute the intent
        return $intent->Execute($this);
    }

}