# Patami Framework

Das Patami Framework ist eine Open Source-Bibliothek für die Heimautomatisierungs-Software [IP Symcon](https://www.symcon.de/) ab Version 4.2.

[Die Dokumentation des Frameworks findest Du hier](https://docs.braintower.de/display/IPSPATAMI).
